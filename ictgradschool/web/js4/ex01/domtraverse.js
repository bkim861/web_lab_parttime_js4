"use strict";

/* Your answer here */


var firstDiv = document.getElementsByClassName('text1');

firstDiv[0].style.color="pink";

var lastDiv = document.getElementById('footer');

lastDiv.style.color = "red";

var secondDiv = document.getElementsByClassName('text2');

secondDiv[0].style.visibility = 'hidden';

var firstParagraph = document.getElementsByTagName('h1')[0];

firstParagraph.innerHTML =  "Hello World";

var myButton = document.getElementById('makeBold');
var changedP = document.getElementsByTagName('p');

function allBold() {

    changedP[0].style.fontWeight = "bold";

}

myButton.onclick = allBold;

